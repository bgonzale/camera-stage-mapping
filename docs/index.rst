Welcome to Camera Stage Mapping's documentation!
================================================
This Python module allows you to calibrate a microscope's stage against the camera.  By moving the sample a known number of steps, and tracking how far the image translates, it is possible to recover the number of steps per pixel.  This code goes a step further, and recovers a 2x2 affine transformation matrix, so it will cope with rotation, flipping, and non-uniform scaling of the X and Y axes.  The calibration routines are designed to be relatively robust and minimally parameterised, so they start by determining a sensible step size automatically.

The main calibration routine is :py:func:`.calibrate_backlash_1d`
which operates in 1D, but can be run twice in orthogonal directions and
then combined with :py:func:`.image_to_stage_displacement_from_1d`.
This is more robust than the 2D grid calibration method.  Both of these
functions are defined in :py:mod:`.camera_stage_calibration_1d`

The underlying cross-correlation tracking code can be configured to use
either FFT or direct correlation (FFT is recommended), and the details 
are in :py:mod:`.fft_image_tracking` or :py:mod:`.correlation_image_tracking`.

Most of the legwork of keeping track of stage and camera coordinates is
done by :py:class:`camera_stage_tracker.Tracker`.

Installing
----------
This package is managed using ``poetry`` and can be installed by cloning the repository and running ``poetry install``.  It is not yet distributed on PyPI, though that's our long term plan.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   hardware_interfacing
   API Reference <autoapi/camera_stage_mapping/index>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
